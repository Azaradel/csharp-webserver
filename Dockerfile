FROM microsoft/dotnet:2.2-sdk as build

WORKDIR /app
COPY *.csproj ./
RUN dotnet restore

COPY . ./
RUN dotnet publish -c Release -o out

FROM microsoft/dotnet:2.2-runtime 

WORKDIR /app
COPY --from=build /app/out .
COPY www www

ENTRYPOINT ["dotnet", "WebServer.dll"]