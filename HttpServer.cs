using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace HttpServer
{
  public class HttpServer
  {
    private HttpListener _server = new HttpListener();
    private string _host;
    private Dictionary<string, Func<HttpListenerContext, Task>> _paths = new Dictionary<string, Func<HttpListenerContext, Task>>();

    public HttpServer(string host)
    {
      _host = host ?? "http://localhost:8080/";
      _server.Prefixes.Add(_host);
      _paths.Add("404", NotFound);
    }

    public async Task Start()
    {
      Console.WriteLine("Starting http server...");
      _server.Start();
      Console.WriteLine("Server listening on: " + _host);
      Console.WriteLine("Possible routes:");

      foreach (var path in _paths)
      {
        Console.WriteLine(" - \"" + path.Key + "\"");
      }

      while (_server.IsListening)
      {
        try
        {
          var ctx = await _server.GetContextAsync();
          await handlePath(ctx);
        }
        catch { }
      }
    }

    public void Stop()
    {
      if (_server.IsListening)
      {
        _server.Stop();
        _server.Close();
      }
    }

    public void AddHandler(string path, Func<HttpListenerContext, Task> func)
    {
      _paths.Add(path, func);
    }

    private static Func<HttpListenerContext, Task> NotFound = async ctx =>
    {
      ctx.Response.StatusCode = 404;
      using (var sw = new StreamWriter(ctx.Response.OutputStream))
      {
        await sw.WriteAsync("");
        await sw.FlushAsync();
      }
    };

    private async Task handlePath(HttpListenerContext ctx)
    {
      var url = ctx.Request.RawUrl;
      if (_paths.ContainsKey(url))
      {
        Console.WriteLine(_paths[url]);
        await _paths[url].Invoke(ctx);
      }
      await NotFound(ctx);
    }
  }
}
