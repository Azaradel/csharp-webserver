﻿using System;
using System.IO;
using System.Net;
using System.Linq;
using System.Threading.Tasks;

namespace HttpServer
{
  class MainClass
  {
    public static async Task Main(string[] args)
    {
      var defaultHost = "http://localhost:8080/";
      var host = System.Environment.GetEnvironmentVariable("APP_HOST");
      var port = System.Environment.GetEnvironmentVariable("APP_PORT");

      if (host != null && port != null)
      {
        defaultHost = host + "/" + port + "/";
      }

      try
      {
        if (File.Exists("/.dockerenv"))
        {
          defaultHost = "http://*/";
        }
      }
      catch { }

      var listener = new HttpServer(defaultHost);
      CatalogHandler("www", "/", ref listener);

      await listener.Start();

      Console.ReadKey();
      listener.Stop();
    }

    public static Func<HttpListenerContext, Task> fileHandler(string path)
    {
      return async ctx =>
      {
        var fs = new FileStream(path, FileMode.Open, FileAccess.Read);
        using (var sw = new StreamWriter(ctx.Response.OutputStream))
        {
          await fs.CopyToAsync(sw.BaseStream);
          await sw.FlushAsync();
        }
        fs.Close();
      };
    }

    public static void CatalogHandler(string catalog, string serverPath, ref HttpServer listener)
    {
      foreach (var file in Directory.GetFiles(catalog).Select(Path.GetFileName).ToArray())
      {
        listener.AddHandler("/" + file, fileHandler(catalog + "/" + file));
      }
      return;
    }
  }
}